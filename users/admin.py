from django.contrib import admin
from .models import userProfile
from django.contrib.auth.admin import UserAdmin
from django.conf import settings
from django.contrib.auth.models import User
# Register your models here.

class profileAdmin(admin.ModelAdmin):
    list_display = ('user', 'matricula', 'is_doctor', 'is_nurse', )
    search_fields = ('matricula','user__first_name', )
    ordering = ('user', )

admin.site.register(userProfile, profileAdmin)