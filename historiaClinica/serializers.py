from rest_framework import serializers
from .models import Paciente, Antecedentes
# create your serializers here!

class pacienteSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source="user.username")

    class Meta:
        model = Paciente
        fields = ('user','first_name', 'last_name',
                'gender', 'birth_day',
                'phone_number', 'civil_status',
                'religion', 'address', 'country', 'city', 'location',
                'scholarship', 'ocupattion', 'ocupattion2', 'email')

class antecedenteSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source="user.username")

    class Meta:
        model = Antecedentes
        fields = ('user', 'paciente' ,'motivo', 'antecedentes', )
