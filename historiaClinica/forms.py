from django import forms
from . import models

class pacienteForm(forms.ModelForm):
    
    class Meta:
        model = models.Paciente
        fields = ('first_name', 'last_name', 
                    'gender', 'birth_day', 
                    'phone_number', 'civil_status',
                    'religion', 'address', 'country', 'city', 'location',
                    'scholarship', 'ocupattion', 'ocupattion2', 'email')
    
    
