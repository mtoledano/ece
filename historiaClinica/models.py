from django.db import models
from core.models import TimeStampedModel
from django.conf import settings
# Create your models here.
GENDER_CHOICES = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
    ('O', 'Otro'),
)
CIVIL_STATUS_CHOICES = (
    ('C', 'Casado(a)'),
    ('S', 'Soltero(a)'),
    ('D', 'Divorciado(a)'),
)
import uuid
# Modelo de Paciente, sera la clase Padre y solo se llamará a las demás clases
# desde su llave foranea 'pk - id'
# Modelo de los datos generales del paciente
class Paciente(TimeStampedModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    udi = models.UUIDField(default=uuid.uuid4, editable=False)
    first_name = models.CharField('Nombre(s)', max_length=100)
    last_name = models.CharField('Apellidos', max_length=100)
    gender = models.CharField('Sexo', max_length=20, choices=GENDER_CHOICES)
    birth_day = models.DateField('Fecha de nacimiento', blank=True, null=True)
    phone_number = models.CharField('Número de telefono', max_length=13)
    civil_status = models.CharField('Estado civil', max_length=20, choices=CIVIL_STATUS_CHOICES)
    religion = models.CharField('Religión', max_length=50, blank=True)
    address = models.CharField('Dirección', max_length=150)
    country = models.CharField('Pais', max_length=40)
    city = models.CharField('Ciudad', max_length=60)
    location = models.CharField('Poblado/Municipio', max_length=100)
    scholarship = models.CharField('Escolaridad', max_length=60, blank=True)
    ocupattion = models.CharField('Ocupación', max_length=100, blank=True)
    ocupattion2 = models.CharField('Otra ocupación', max_length=100, blank=True, null=True)
    email = models.EmailField('Correo Electrónico', max_length=50)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def get_age(self):
        import datetime
        return int( (datetime.date.today() - self.birth_day).days / 365.25 )
    class Meta:
        permissions = (
            ('can_create_paciente', 'Puede crear o dar de altu un paciente'),
            ('can_delete_paciente', 'Puede eliminar pacientes'),
            ('can_update_paciente', 'Puede actualizar pacientes'),
        )

# Modelo Antecedentes del paciente, la cual almacenara toda la información
# acerca de enfermedades pasadas de él o de sus familiares, así como inmunizaciones
# hábitos y alergias
class Antecedentes(TimeStampedModel):
    """
    Modelo de motivo y antecedentes de la enfermedad presentada en el momento de
    la consulta
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE, null=True) #se cambió)
    motivo = models.TextField('Motivo de la consulta')
    antecedentes = models.TextField('Antecedentes de la enfermedad actual', blank=True, null=True)

    def __str__(self):
        return 'Antecedentes del Paciente - %s' % (self.paciente.get_full_name())

CAUSA_CHOICES = (
    ('V', 'Vivos'),
    ('F', 'Fallecidos'),
)
TRUE_FALSE_CHOICES = (
    ('P', 'Positivo'),
    ('N', 'Negativo'),
)
class AntecedentesHeredofamiliares(TimeStampedModel):
    """
    Modelo de Antecedentes del mismo paciente y/o de sus familiares
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE, null=True) #se cambió)
    parents = models.CharField('Padres', max_length=20, choices=CAUSA_CHOICES)
    parents_causa = models.CharField('Causa de muerte', max_length=150, blank=True, null=True, default='Vivos')
    brothers = models.CharField('Hermanos', max_length=20, choices=CAUSA_CHOICES)
    brothers_causa = models.CharField('Causa de muerte', max_length=150, blank=True, null=True, default='Vivos')
    sons = models.CharField('Hijos', max_length=20, choices=CAUSA_CHOICES)
    sons_causa = models.CharField('Causa de muerte', max_length=150, blank=True, null=True, default='Vivos')
    DBT = models.CharField('Diabetes mellitus', max_length=20, choices=TRUE_FALSE_CHOICES)
    HTA = models.CharField('HiperTensión Arterial', max_length=20, choices=TRUE_FALSE_CHOICES)
    TBC = models.CharField('Tuberculosis', max_length=20, choices=TRUE_FALSE_CHOICES)
    GM = models.CharField('Gemelar', max_length=20, choices=TRUE_FALSE_CHOICES)
    other = models.CharField('Otra(s)', max_length=200, blank=True, null=True)

    def __str__(self):
        return 'Antecedentes Heredofamiliares de - %s' % (self.paciente.get_full_name())

class antecedentesPersonales(TimeStampedModel):
    """
    Modelo de los antecedentes personales del paciente
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE, null=True) #se cambió)
    alcohol = models.CharField('Alcohol', max_length=100, blank=True)
    tabaco = models.CharField('Tabaco', max_length=100, blank=True)
    drogas = models.CharField('Drogas', max_length=100, blank=True)
    infusiones = models.CharField('Infusiones', max_length=100, blank=True)

    def __str__(self):
        return 'Antecedentes personales de %s' % (self.paciente.get_full_name())

class Fisiologicos(TimeStampedModel):
    """
    Antecedentes personales Fisiologicos del paciente, este modelo hereda de
    antecedentesPersonales
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    antecedente = models.ForeignKey(antecedentesPersonales, on_delete=models.CASCADE, blank=True, null=True) #se cambió)
    dipsia = models.CharField('Dipsia', max_length=250, blank=True)
    diuresis = models.CharField('Diuresis', max_length=250, blank=True)
    catarsis = models.CharField('Catarsis', max_length=250, blank=True)
    somnia = models.CharField('Somnia', max_length=250, blank=True)
    others = models.CharField('Otros', max_length=250, blank=True)

    def __str__(self):
        return 'Antecedentes personales fisiológicos de %s' % (self.antecedente.paciente.get_full_name())

class Patologicos(TimeStampedModel):
    """"
    Antecedentes personales patológicos del paciente, esta modelo hereda de
    antecedentesPersonales
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    antecedente = models.ForeignKey(antecedentesPersonales, on_delete=models.CASCADE, null=True) #se cambió)
    infancia = models.CharField('Infancia', max_length=200)
    adulto = models.CharField('Adulto', max_length=200)
    DBT = models.CharField('Diabetes mellitus', max_length=20, choices=TRUE_FALSE_CHOICES)
    HTA = models.CharField('HiperTensión Arterial', max_length=20, choices=TRUE_FALSE_CHOICES)
    TBC = models.CharField('Tuberculosis', max_length=20, choices=TRUE_FALSE_CHOICES)
    GM = models.CharField('Gemelar', max_length=20, choices=TRUE_FALSE_CHOICES)
    other = models.CharField('Otra(s)', max_length=200, blank=True, null=True)
    quirurgicos = models.CharField('Quirúrgicos', max_length=200)
    traumalogicos = models.CharField('Traumalógicos', max_length=200)
    alergicos = models.CharField('Alérgicos', max_length=200)
    others = models.CharField('Otra(s)', max_length=200, blank=True, null=True)

    def __str__(self):
        return 'Antecedentes personales patológicos de %s' % (self.antecedente.paciente.get_full_name())

class signosVitales(TimeStampedModel):
    """
    Modelo de signos vitales del paciente
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE, null=True) #se cambió
    pulso = models.FloatField('Frecuencia cardiaca por minuto', default=0.0)
    temperatura = models.FloatField('Temperatura en °C', default=0.0)
    respiracion = models.PositiveSmallIntegerField('Respiración', default=0)
    arterial = models.CharField('Presión Arterial', max_length=7, default='0/0')

    def __str__(self):
        return 'Signos vitales del paciente - %s' % (self.paciente.get_full_name())

    def get_pulso(self):
        if self.pulso >= 60 and self.pulso <= 80:
            return 0
        elif self.pulso >= 80 and self.pulso <= 100:
            return 1
        elif self.pulso > 100:
            return 2
        return self.pulso
