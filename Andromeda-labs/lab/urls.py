from django.conf.urls import url
from . import views

urlpatterns = [
    # API URLS
    url(r'^estudio-create/$', views.estudioCreateList.as_view(), name='estudio-create'),
    url(r'^estudio-detail/(?P<pk>[0-9]+)/$', views.estudioDetail.as_view(), name='estudio-detail'),
    url(r'^check-estudio/$', views.check_estudios.as_view(), name='check-estudio'),
    url(r'^get_estudios/(?P<pk>[0-9]+)/$', views.getPacienteEstudio.as_view(), name='get_paciente_estudios'),
]
