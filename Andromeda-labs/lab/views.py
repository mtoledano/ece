from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404

from rest_framework import viewsets
from .serializers import estudioSerializer, estudioListSerializer
from .models import Estudio, EstudioList

from rest_framework.permissions import IsAuthenticated
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
# Create your views here.

### API VIEWS ###
class estudioCreateList(generics.ListCreateAPIView):
    queryset = Estudio.objects.all()
    serializer_class = estudioSerializer
    permission_classes = (IsAuthenticated, )

    def list(self, request):
        queryset = self.get_queryset()
        serializer = estudioSerializer(queryset, many=True)
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class estudioDetail(APIView):

    def get_object(self, pk):
        try:
            return Estudio.objects.get(pk=pk)
        except Estudio.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        estudio = self.get_object(pk)
        serializer = estudioSerializer(estudio)
        return Response(serializer.data)
    def put(self, request, pk, format=None):
        estudio = self.get_object(pk)
        serializer = estudioSerializer(estudio, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def delete(self, request, pk, format=None):
        estudio = self.get_object(pk)
        estudio.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class check_estudios(generics.ListAPIView):
    queryset = EstudioList.objects.all()
    serializer_class = estudioListSerializer
    permission_classes = (IsAuthenticated, )

    def list(self, request):
        queryset = self.get_queryset()
        serializer = estudioListSerializer(queryset, many=True)
        return Response(serializer.data)

class getPacienteEstudio(generics.ListAPIView):
    serializer_class = estudioSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        paciente = self.kwargs.get('pk')
        return Estudio.objects.filter(paciente=paciente)
### NORMAL DJANGO VIEWS ###
from django.http import JsonResponse

from django.contrib.auth.views import login_required
@login_required
def indexLab(request):
    estudios = EstudioList.objects.all()
    return render(request, 'lab/laboratorio.html', {'section':'lab', 'estudios':estudios})
