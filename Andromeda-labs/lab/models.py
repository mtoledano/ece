from django.db import models
from django.conf import settings
import datetime

from historiaClinica.models import Paciente
# Create your models here.
ESTUDIO_CHOICES = (
    ('BHC', 'Recuento de células en sangre'),
    ('PQS', 'Panel de quimica sanguinea'),
    ('CS', 'Cultivo de sangre'),
    ('EFH', 'Exámen de funcionamiento de hígado'),
    ('PE', 'Pruebas de embarazo'),
    ('TM', 'Triple marcador'),
    ('ERN', 'Exámen de los recien nacidos'),
    ('NB', 'Nivel de bilirrubina'),
    ('VIH', 'VIH - SIDA'),
    ('ES', 'Electrolitos en la sangre'),
    ('GC', 'Glucosa'),
    ('HG', 'Hemograma'),
    ('MM', 'Mamografía'),
)

class EstudioList(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField('Nombre del estudio', max_length=200, blank=True)
    description = models.TextField('Descripción', blank=True, null=True)

    def __str__(self):
        return self.name

class Estudio(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    nEstudio = models.ForeignKey(EstudioList, blank=True, null=True)
    estudioDetalle = models.TextField("Detalle del estudio", blank=True, null=True)
    nurseNotes = models.TextField("Notas (observaciones)", max_length=200, blank=True, null=True)
    is_normal = models.BooleanField(default=False)
    is_danger = models.BooleanField(default=False)
    is_good = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s - %s' %(self.paciente, self.nEstudio)

    def set_normal(self):
        self.is_normal = True
        self.is_danger = False
        self.is_good = False
        self.save()

    def set_danger(self):
        self.is_normal = False
        self.is_danger = True
        self.is_good = False
        self.save()

    def set_good(self):
        self.is_normal = False
        self.is_danger = False
        self.is_good = True
        self.save()
