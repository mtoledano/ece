from django.conf.urls import url
from . import views
from lab.views import indexLab

urlpatterns = [
    url(r'^$', views.dashboard, name='dashboard'),
    url(r'^historia-clinica/$', views.historiaClinicaView.as_view(), name='historia_clinica'),
    url(r'^laboratorio/$', indexLab, name='laboratorio'),
    #url(r'^paciente-create/$', views.createPaciente, name='paciente_create'),
    url(r'^pacientes/$', views.pacientesList.as_view(), name='pacientes_list'),
    url(r'^api/paciente-detail/(?P<pk>[0-9]+)/$', views.pacienteDetail.as_view(), name='paciente_detail'),
    url(r'^api/antecedentes/(?P<pk>[0-9]+)/$', views.antecedenteList.as_view(), name='antecedentes_detail'),
    url(r'^pacientes/detail/(?P<pk>[0-9]+)$', views.pacienteDetail2, name='paciente_detail2'),
]
